# SignIn - SignUp - SignOut

Le but de cet exercice est de développer un mécanisme d'authentification d'utilisateurs, et de création de compte.

Les utilisateurs doivent pouvoir :

* créer un compte avec un _name_, un _email_, et un _password_ (sign up*)
* se connecter avec un _email_ et un _password_ (*sign in*)
* accéder à une page web affichant leurs informations
* se déconnecter (*sign out*)

## Création de votre repository

Commencez par créer votre repository GitLab en
suivant [ce lien](https://gitlab-classrooms.cleverapps.io/assignments/84b37f88-036a-49c4-bdf7-aaef11f2ce61/accept)

> ℹ : ce repository servira de rendu pour votre exercice. Vous pouvez le cloner localement pour travailler. Pensez à
> faire des `git commit` régulièrement, et n'oubliez pas de faire un `git push` pour envoyer vos modifications.

Ce repository contient un squelette de code sommaire, les fichiers de test `.feature` qui permettront de tester votre
travail, ainsi que la configuration du pipeline `.gitlab-ci.yml`.

## Énoncé

* L'application doit pouvoir démarrer en utilisant la commande `mvn spring-boot:run` à la racine du repository.
* L'application doit écouter sur le port `8080`.
* L'application doit sauvegarder ses données dans une base de données `h2` embarquée en mémoire

### Utilisateurs pré-enregistrés

Les tests supposent que 2 utilisateurs existent déjà dans la base de données, ayant les informations suivantes :

| name       | email                         | password        |
|------------|-------------------------------|-----------------|
| Luke       | luke@rebels.org               | yoda-forever    |
| Dark Vador | lord-vador@galatic-empire.com | luke-leia-padme |

Ces utilisateurs doivent être chargés en base de données au démarrage de l'application.
Les mots de passe doivent être stockés encodés en base de données. Vous pouvez réutiliser le code d'encodage MD5 ou
HMAC.
Les mots de passe dans le tableau-ci dessus sont non-encodés, vous devez les encoder avant de les stocker.

### Home

* La route `GET /` retourne une réponse HTTP de type `text/html`, présentant une page web dont le titre (`title`)
  est `Home`. Cette page contient 3 liens vers les pages (le contenu précis des pages est détaillé plus bas) :
    * `/signIn` contenant le formulaire de login
    * `/signUp` contenant le formulaire de création de compte
    * `/userInfo` contenant les informations de l'utilisateur

### Sign In

Sign In correspond à l'action de se connecter à l'application.

* La page '/signIn' retourne une page web dont le titre est `Sign In`. Cette page contient un formulaire contenant :
    * un champ de saisie email nommé `email`
    * un champ de saisie de mot de passe nommé `password`
    * un bouton de soumission
* La soumission du formulaire envoie une requête `POST /signIn`
* Si le login ou le mot de passe est incorrect, le formulaire renvoie une page web dont le titre est `Error`. Cette page
  contiendra le message `User or Password is incorrect`
* Si le login et le mot de passe sont corrects, le formulaire redirige vers la page `/userInfo`, avec un code de
  redirection HTTP `302`.
* Si le login et le mot de passe sont corrects, une session HTTP est créée côté serveur, permettant d'identifier quel
  est l'utilisateur actuellement connecté.

### User Info

* Si aucun utilisateur n'est connecté (session nouvelle ou vide), la page `/userInfo` redirige vers la page `/signIn`
  avec un code de redirection HTTP `302`.
* La page '/userInfo' retourne une page web dont le titre est `User Info - %name%`, avec `%name%` étant le nom de
  l'utilisateur connecté.
* La page '/userInfo' affiche le nom de l'utilisateur, ainsi que son adresse email, mais pas son mot de passe.
* Cette page contient également un lien vers le page `/signOut` pour se déconnecter.

### Sign Up

Sign Up correspond à l'action de se créer un compte dans l'application.

* La page '/signUp' retourne une page web dont le titre est `Sign Up`. Cette page contient un formulaire contenant :
    * un champ de saisie de nom, nommé `name`
    * un champ de saisie email nommé `email`
    * un champ de saisie de mot de passe nommé `password`
    * un bouton de soumission
* La soumission du formulaire envoie une requête `POST /signUp`
* Si l'email saisi par l'utilisateur existe déjà en base de données, le formulaire renvoie une page web dont le titre
  est `Error`. Cette page contiendra le message `User already exists`.
* Sinon, l'utilisateur est sauvegardé en base de données, une session HTTP est créée côté serveur, permettant
  d'identifier quel est l'utilisateur actuellement connecté, et l'utilisateur est redirigé vers la page `/userInfo`,
  avec un code de redirection HTTP `302` (comme s'il venait de se connecter)

### Sign Out

Sign Out correspond à une action de déconnexion.

* Si aucun utilisateur n'est connecté (session nouvelle ou vide), la page `/signOut` redirige vers la page `/`
  avec un code de redirection HTTP `302`.
* Si un utilisateur est connecté (en session HTTP), la page `/signOut` invalide la session active, et redirige vers la page `/`
  avec un code de redirection HTTP `302`.

## Évaluation de votre exercice

Votre repository GitLab contient déjà tous les éléments nécessaires à l'évaluation de votre exercice.

L'évaluation de votre exercice se fait grâce à un pipeline GitLab-CI.

Pour vérifier votre rendu, rendez-vous dans votre projet GitLab, dans la section _CI/CD - Pipelines_.
Votre rendu peut aussi être consulté dans votre dashboard sur [GitLab Classrooms](https://gitlab-classrooms.cleverapps.io/).

## Tester localement votre exercice

Vous pouvez également tester localement votre exercice avec la procédure suivante:

* Téléchargez le fichier [`cucumber-runner.jar`](https://gitlab.univ-lille.fr/api/v4/projects/gitlab-classrooms%2Fcucumber-runner/packages/generic/cucumber-runner/0.0.1/cucumber-runner.jar) à la racine de votre projet, ou avec la commande suivante :

```bash
curl -s -o cucumber-runner.jar -L https://gitlab.univ-lille.fr/api/v4/projects/gitlab-classrooms%2Fcucumber-runner/packages/generic/cucumber-runner/0.0.1/cucumber-runner.jar
```

* Exécutez les tests en lançant dans un terminal, la commande suivante :

```bash
java -jar cucumber-runner.jar src/main/resources/features
```

> ⚠️ : il vous faut une version minimale de java 11 pour exécuter le `cucumber-runner`
>
> Vérifiez votre version de java avec la commande `java --version`
