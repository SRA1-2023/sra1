# Systèmes répartis avancés - 1 (SRA1)

Site de l'UE SRA1 du [Master Informatique](https://sciences-technologies.univ-lille.fr/informatique/formation/master-informatique) de l'Université de Lille.

1. Semaine 1 : [Les applications client-serveur 3-tier avec Jakarta EE et Spring](week1)
1. Semaine 2 : [La couche d'accès aux données avec JPA](week2)
1. Semaine 3 : [La couche de présentation](week3)
1. Semaines 4, 5 et 6 : [Projet](project/README.md)

# Pour aller plus loin

- [Cucumber](bonus%2Fcucumber.md)
- [DevOps](bonus/devops.md)
- [Docker](bonus/docker.md)
- [Intégration et déploiement continus](bonus/ci-cd.md)

Si vous avez besoin de réviser quelques pré-requis pour ce cours, vous pouvez consulter aussi les ressources suivantes sur :

- les [annotations Java](bonus/annotations.md),
- l'[API Collection](bonus/collection.md),
- les [types génériques](bonus/generics.md),
- la [réflexivité](bonus/reflection.md).


# Si vous souhaitez vérifier vos notes

Vous pouvez visualiser l'ensemble des notes de vos projets ainsi que de vos QCM sur le site https://gitlab-classrooms.cleverapps.io/
Cela permet de s'assurer que le comportement en local de vos tests est bien identique sur le pipeline, si vous constatez une différence, n'hésitez pas à nous solliciter pour vérifier qu'il ne s'agit pas d'un bug sur la build.

# Nous contacter

Sur discord : https://discord.gg/R4jQACyD

Par email : 

* maximilien.chevalier@univ-lille.fr
* julien.wittouck@univ-lille.fr
* guillaume.dufrene@univ-lille.fr
* lionel.seinturier@univ-lille.fr
