# Comprendre l'Inversion de Contrôle (IoC) avec Spring Boot

## Objectif

Comprendre le concept de l'Inversion de Contrôle (IoC) en utilisant Spring Boot pour l'injection de dépendances, en manipulant des classes de chiffrement Hmac et MD5 à travers une interface commune.

## Introduction à l'Inversion de Contrôle (IoC)

L'Inversion de Contrôle (IoC) est un principe fondamental dans le développement d'applications basées sur des frameworks tels que Spring Boot.
Au lieu de créer et gérer directement les objets et leurs dépendances, l'IoC transfère la responsabilité de la création et de la gestion des objets à un conteneur d'inversion de contrôle (IoC container) tel que Spring.
Le conteneur IoC s'occupe de l'instantiation des objets, de l'injection de leurs dépendances et de leur configuration, ce qui permet de rendre l'application plus souple, maintenable et testable.

## Etapes communes

### Création du projet

Commencez par créer votre repository GitLab en suivant [ce lien](https://gitlab-classrooms.cleverapps.io/assignments/85a45c97-7d56-4f33-ad04-bb13b7e8620c/accept)

> ℹ : ce repository servira de rendu pour votre exercice. Vous pouvez le cloner localement pour travailler. Pensez à faire des `git commit` régulièrement, et n'oubliez pas de faire un `git push` pour envoyer vos modifications.

Ce squelette de projet contiendra les dépendances nécessaires pour l'injection de dépendances avec Spring Boot ainsi que la dépendance Spring Web.
L'application exposera un RestController avec une méthode `GET /encrypt?message=` qui retournera la chaîne de caractères passée en paramètre sous un format chiffrée.
Nous allons ensuite implémenter les différentes étapes en respectant les consignes de chaque exercice ci-dessous.
Vous devrez créer toutes les classes dans le même package que la classe principale.

### Classes de chiffrement

Nous allons utiliser deux classes de chiffrement fournies pour illustrer l'injection de dépendances.

#### Encoder

On va commencer par créer une interface `Encoder` avec une méthode `encode` prenant en paramètre une chaîne de caractères à chiffrer.

```java
public interface Encoder {

    String encode(String message);
}
```

#### Hmac

La classe `Hmac` permet de chiffrer une chaîne de caractères avec l'algorithme HmacSHA256.

```java
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class Hmac implements Encoder {

    private static final String SECRET_KEY = "mySecretKey";

    public String encode(String message) {
        try {
            SecretKeySpec signingKey = new SecretKeySpec(SECRET_KEY.getBytes(StandardCharsets.UTF_8), "HmacSHA1");
            Mac mac = Mac.getInstance("HmacSHA1");
            mac.init(signingKey);
            byte[] rawHmac = mac.doFinal(message.getBytes());
            return Base64.getEncoder().encodeToString(rawHmac);
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to generate Hmac : " + e.getMessage());
        }
    }
}
```

#### MD5

La classe `MD5` permet de chiffrer une chaîne de caractères avec l'algorithme MD5.

```java
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class MD5 implements Encoder {

    public String encode(String message) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] bytesOfMessage = message.getBytes(StandardCharsets.UTF_8);
            md.update(bytesOfMessage);
            return Base64.getEncoder().encodeToString(md.digest());
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Failed to generate MD5 : " + e.getMessage());
        }
    }
}
```

## Exercices

### Exercice 1

Injection de dépendance manuelle

1. Créez une classe `ChiffrementService` avec une méthode `chiffrer` prenant en paramètre une chaîne de caractères à chiffrer.
2. Ajoutez un attribut de classe encoder de type Encoder et un constructeur pour initialiser cet attribut.
3. Définissez la méthode `chiffrer` qui appelle la méthode `encode` de l'attribut encoder.
4. L'injection de dépendance sera réalisée manuellement, c'est-à-dire que vous devrez instancier vous-même les objets et gérer leurs dépendances.
5. Créez une instance de `ChiffrementService` dans votre RestController avec la dépendance nécessaire.
6. Retournez le résultat de la méthode `chiffrer` dans la méthode `GET /encrypt?message=` de votre RestController.

Votre projet devrait ressembler à ceci :

```
fr.univ.spring.etape1
    |_ ChiffrementController
    |_ ChiffrementService
    |_ Encoder
    |_ Etape1Application
    |_ Hmac
    |_ MD5
```

### Exercice 2

Nous allons maintenant déléguer la création et la gestion des objets à Spring via l'injection de dépendances.

1. Créez une classe de configuration `ChiffrementConfig` annotée avec `@Configuration`.
2. Déclarez une méthode `encoder` dans la classe `ChiffrementConfig`, annotée avec `@Bean`, avec un type de retout `Encoder` et qui retourne une instance de `Hmac`.
3. Annotez la classe `ChiffrementService` avec le stéréotype `@Service` pour indiquer à Spring qu'il s'agit d'un composant métier.
4. L'utilisation de Spring Boot facilite l'injection de dépendances grâce à l'annotation `@Bean`, qui indique à Spring de gérer la création et la configuration des objets.
5. Modifiez votre RestController pour injecter la dépendance `ChiffrementService` avec l'annotation `@Autowired` dans un attribut de classe.
6. Utilisez l'objet `ChiffrementService`, à la place de l'instantiation utilisée dans l'étape 1, obtenu à partir du contexte pour appeler la méthode `chiffrer` avec une chaîne de caractères de votre choix.

Votre projet devrait ressembler à ceci :

```
fr.univ.spring.etape2
    |_ ChiffrementConfig
    |_ ChiffrementController
    |_ ChiffrementService
    |_ Encoder
    |_ Etape2Application
    |_ Hmac
    |_ MD5
```

Vous constaterez que l'injection de dépendances est plus simple avec Spring Boot, car il n'est plus nécessaire de gérer les dépendances manuellement.

### Exercice 3

La problématique est la suivante : nous souhaitons pouvoir choisir l'implémentation de chiffrement à utiliser en fonction du contexte d'exécution.
Par exemple, nous souhaitons utiliser l'algorithme Hmac en développement et l'algorithme MD5 en production.

Nous allons utiliser les profils Spring Boot pour choisir l'implémentation de chiffrement à utiliser au démarrage de l'application.

1. Modifiez la classe `ChiffrementConfig` pour définir deux méthodes `chiffrementService` (avec un nom différent pour permettre la compilation), une pour Hmac et une pour MD5, et les annoter avec `@Profile("Hmac")` et `@Profile("MD5")` respectivement.
2. Les profils Spring Boot permettent de spécifier différents comportements d'application en fonction de l'environnement ou de la configuration. Dans notre cas, nous utiliserons les profils pour choisir l'implémentation de chiffrement souhaitée.
3. Modifiez le fichier `application.properties` pour définir le profil actif souhaité (`spring.profiles.active=Hmac` ou `spring.profiles.active=MD5`) en fonction de l'implémentation de chiffrement à utiliser.
4. Démarrez l'application et vérifiez que l'implémentation de chiffrement utilisée est bien celle définie dans le profil actif.
