# Contrôleur 101

Le but de cet exercice est de développer un contrôleur avec deux routes : une route GET et une route POST.

## Création de votre repository

Commencez par créer votre repository GitLab en suivant [ce lien](https://gitlab-classrooms.cleverapps.io/assignments/07631218-f1f0-4dc2-ab4f-3f5d0099bf14/accept)

> ℹ : ce repository servira de rendu pour votre exercice. Vous pouvez le cloner localement pour travailler. Pensez à faire des `git commit` régulièrement, et n'oubliez pas de faire un `git push` pour envoyer vos modifications.

## Création de votre projet

Pour cet exercice, vous devez créer un squelette de projet sur le site [Spring Initializr](https://start.spring.io/).
Ajoutez la dépendance `Spring Web` à votre projet afin de pouvoir créer un contrôleur.
La racine du projet généré sur classroom doit contenir le dossier `src` et le fichier `pom.xml`.

## Énoncé

* À la racine de votre repository, créez une nouvelle application *Spring-Boot*, compatible *Java 17*, utilisant *Maven* comme système de build.
* L'application doit pouvoir démarrer en utilisant la commande `mvn spring-boot:run` à la racine du repository.
* L'application doit écouter sur le port `8080`.
* La route `GET /` retourne une réponse HTTP de type `text/plain`, avec comme `Body` la chaîne de caractères `Hello World`.
* La route `GET /signIn` retourne une réponse HTTP de type `test/html`, avec un formulaire comprenant :
  * un champ de saisie email nommé `email`
  * un champ de saisie de nom, nommé `name` 
  * un champ de saisie de mot de passe nommé `password`
  * un champ de saisie de validation de mot de passe nommé `password_verify`
* La soumission du formulaire envoie une requête `POST /signIn`
* La route `POST /signIn` renvoie une réponse de type `application/json`, contenant l'email saisi et le mot de passe dans 2 champs `email` et `encryptedPassword`. La valeur de `encryptedPassword` correspond au champ `password` encodé en _MD5_.
  * si les champs `password` et `password_verify` ont des valeurs différentes, la route `POST /signin` renvoie un code d'erreur HTTP `400 Bad Request`.

## Évaluation de votre exercice

Afin de pouvoir évaluer votre exercice, suivez les étapes suivantes :
* Ajoutez le fichier [controler-101.feature](controler-101.feature) dans le répertoire `src/main/resources/features` de votre repository
* Ajoutez le fichier [.gitlab-ci.yml](.gitlab-ci.yml) à la racine de votre repository (attention au nommage du fichier qui doit bien démarrer par un `.` : `.gitlab-ci.yml`)
* Pushez votre code et ces deux nouveaux fichiers dans votre repository

L'évaluation de votre exercice se fait grâce à un pipeline GitLab-CI.

Pour vérifier votre rendu, rendez vous dans votre projet GitLab, dans la section _CI/CD - Pipelines_.
Votre rendu peut aussi être consulté dans votre dashboard sur [GitLab Classrooms](https://gitlab-classrooms.cleverapps.io/).

## Tester localement votre exercice

Vous pouvez également tester localement votre exercice avec la procédure suivante:

* Téléchargez le fichier [`cucumber-runner.jar`](https://gitlab.univ-lille.fr/api/v4/projects/gitlab-classrooms%2Fcucumber-runner/packages/generic/cucumber-runner/0.0.1/cucumber-runner.jar) à la racine de votre projet, ou avec la commande suivante : 

```bash
curl -s -o cucumber-runner.jar -L https://gitlab.univ-lille.fr/api/v4/projects/gitlab-classrooms%2Fcucumber-runner/packages/generic/cucumber-runner/0.0.1/cucumber-runner.jar
```

* Exécutez les tests en lançant dans un terminal, la commande suivante : 

```bash
java -jar cucumber-runner.jar src/main/resources/features
```

> ⚠️ : il vous faut une version minimale de java 11 pour exécuter le `cucumber-runner`
>
> Vérifiez votre version de java avec la commande `java --version`
