Feature: Controller 101

  Background:
    Given The server listens on port 8080

  Scenario: GET "/" returns "Hello World"
    When HTTP request GET "/" is send
    Then HTTP response code is 200
    And  HTTP response has header matching "Content-Type: text/plain"
    And  HTTP response as body equal to "Hello World"

  Scenario: GET "/signIn" returns a form
    When I navigate to "http://localhost:8080/signIn"
    Then element having name "email" should be present
    Then element having name "name" should be present
    Then element having name "password" should be present
    Then element having name "password_verify" should be present
    Then element having name "submit" should be present

  Scenario: POST "/signIn" fails with 400 with different passwords
    When HTTP request POST "/signIn" is send with form parameters "name=john&email=john%40univ-lille.fr&password=toto&password_verify=tata"
    Then HTTP response code is 400

  Scenario: POST "/signIn" success
    When HTTP request POST "/signIn" is send with form parameters "name=john&email=john%40univ-lille.fr&password=toto&password_verify=toto"
    Then HTTP response code is 200
    And  HTTP response has header matching "Content-Type: application/json"
    And  HTTP response as body equal to '{"email":"john@univ-lille.fr","encryptedPassword":"9x2+UmKKP4OnerSUgXUlxg=="}'

