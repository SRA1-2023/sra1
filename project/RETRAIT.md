# Retrait des commandes.

## En tant qu'admin du drive, je peux gérer des quais de livraison pour organiser le retrait des commandes.

L'admin pourra voir, ajouter, modifier ou supprimer des quais de livraison.

Un quai de livraison sera simplement identifié par son numéro (1 .. 30).

Afin de les représenter graphiquement sur un plan, nous leur associerons également une coordonnée (x, y) et une taille (longueur, largeur).

Pour simplifier la représentation, les emplacements seront forcément alignés avec les axes x et y (pas de rotation).

Une page permettra de visualiser ce plan des quais.

## En tant que client, je peux me présenter à une borne pour retirer ma commande.

Le client arrive devant une borne qui lui propose de saisir son numéro de client.

Le système recherche la commande, et, si elle est prête à être livrée, assigne un numéro de quai disponible au client.

Vous réaliserez les écrans proposant cette interface d'accueil et d'orientation des clients.

Vous mettrez évidemment à jour les données nécessaires au traitement et à la livraison de la commande et des quais.

## En tant qu'employé, je peux visualiser la liste des commandes dont la livraison peut être effectuée afin de remettre les articles à un client.

Si ce n'est pas déjà fait, l'employé pourra sélectionner son nom parmi une liste déroulante (issue d'une table des employés).

Cette information pourra être conservée en session et l'employé pourra se "déconnecter" quand il le souhaite.

L'employé choisit l'une des commandes pour laquelle il peut livrer les produits à un client en attente.

## En tant qu'employé, je peux scanner un ticket de livraison afin de m'assurer que le bon client est au quai et commencer la procédure de livraison.

Lorsque le client s'enregistre à la borne il faut lui imprimer un ticket avec le numéro de quai et un code barre unique qui identifie cette livraison.

Dans le cadre de cette fonction, vous ferez une vue qui affiche le numéro de quai et génère un code barre.

On considèrera qu'il sera ensuite facile d'imprimer cette page avec une imprimante intégrée à la borne.

Réaliser un 2ème écran, visible par l'employé permettant de saisir le numéro de code barre de livraison.

Après saisie du code, les informations suivantes apparaissent : numéro de commande, statut courant, nom du client, numéro du quai.

Un dernier écran permet à l'employé de valider la livraison au client, ce qui change le statut de la commande et libère le quai pour être utilisé pour une autre livraison.

## En tant qu'admin, je peux avoir des statistiques sur les commandes retirées.

L'admin doit pouvoir visualiser le nombre de commandes livrées par heure, pour une journée choisie.

Un compteur reprendra aussi le nombre total de commandes livrées et le chiffre d'affaire de la journée (toutes les commande livrées).

Une autre partie de la page montrera le nombre de commandes livrées par employé et par heure (par tranche d'une heure).
