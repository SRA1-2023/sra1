# Projet

## Livraison

Le projet est à rendre pour le 25 octobre 2023.

Les sources seront récupérées à 23h00 (heure de Paris).

Vous avez déjà été ajoutés au repository de votre groupe sur GitLab, dans un projet situé dans ce groupe : https://gitlab.univ-lille.fr/SRA1-2023/Classroom-SRA1-2023/projects

> ℹ : configurez convenablement votre environnement git pour faire paraitre votre nom, prénom, email dans vos commits.
> utilisez les commandes suivantes : `git config user.name ""` et `git config user.email ""`. Utilisez bien votre adresse mail `.etu@univ-lille.fr`.

Si vous travaillez avec des branches et des _merge-request_, évitez les _squash_ et _squash-commit_. Privilégiez des _merge_ classique ou des _rebase_.

## Contraintes

Votre projet **doit** utiliser Spring et Maven.

Votre application devra pouvoir se lancer en faisant :

- un `git clone` de votre projet,
- un démarrage avec la commande `spring-boot:run` ou via `docker-compose`

Les informations seront persistées à travers des objets définis avec les annotation JPA dans une base de données relationnelle.
Au choix : MySQL, PostgreSQL ou H2 Database.

Votre projet devra initialiser la base de données avec un schéma et des données si nécessaire (par exemple à l’aide d’un script SQL).

Pensez à mettre un fichier texte "README.md". (nom et prénom de chacun, sujet, procédure d'installation et d'initialisation, difficultés rencontrées, choix particuliers)

## Groupes constitués

| Groupe         | Sujet                             | Étudiants                                                                                                                                                                                   |
| -------------- | --------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| A (GL)         | [CLIENTS](CLIENTS.md)             | benjamin.douchet.etu@univ-lille.fr<br/>matheo.gallego.etu@univ-lille.fr<br/>hugo.herman.etu@univ-lille.fr<br/>jeanne.lauwers.etu@univ-lille.fr<br/>zotahina.ratefiarivony.etu@univ-lille.fr |
| B (GL)         | [CRENEAUX](CRENEAUX.md)           | mathieu.bachelet.etu@univ-lille.fr<br/>amel.cherief.etu@univ-lille.fr<br/>romain.degrave.etu@univ-lille.fr<br/>remi.dufloer.etu@univ-lille.fr<br/>adrien.vanegue.etu@univ-lille.fr          |
| C (GL)         | [REFERENCEMENT](REFERENCEMENT.md) | virginie.amand.etu@univ-lille.fr<br/>antoine.dupont4.etu@univ-lille.fr<br/>kelean.gallet.etu@univ-lille.fr<br/>alexandre.ledun.etu@univ-lille.fr                                            |
| D (GL)         | [PROMOTIONS](PROMOTIONS.md)       | thomas.delcroix2.etu@univ-lille.fr<br/>maxime.deronne.etu@univ-lille.fr<br/>leo.levancanhditban.etu@univ-lille.fr<br/>thimo.revolbauz.etu@univ-lille.fr                                     |
| E (E-SERVICES) | [CLIENTS](CLIENTS.md)             | yaoisidore.amevigbe.etu@univ-lille.fr<br/>etienne.dhaussy.etu@univ-lille.fr<br/>aboubakarsiriki.diakite.etu@univ-lille.fr<br/>lucas.ple.etu@univ-lille.fr                                   |
| F (E-SERVICES) | [CRENEAUX](CRENEAUX.md)           | churchill.atchedji.etu@univ-lille.fr<br/>arnaud.delberghe.etu@univ-lille.fr<br/>zakaria.elkhayari.etu@univ-lille.fr<br/>nassima.kesraoui.etu@univ-lille.fr                                  |
| G (E-SERVICES) | [REFERENCEMENT](REFERENCEMENT.md) | remi.dewame.etu@univ-lille.fr<br/>gaelle.fret.etu@univ-lille.fr<br/>geoffrey.herman.etu@univ-lille.fr<br/>ayoub.lahouaichri.etu@univ-lille.fr                                               |
| H (E-SERVICES) | [PROMOTIONS](PROMOTIONS.md)       | madyson.caron.etu@univ-lille.fr<br/>noe.chachignot.etu@univ-lille.fr<br/>aurelien.plancke.etu@univ-lille.fr<br/>nada.zine.etu@univ-lille.fr                                                 |
| I (E-SERVICES) | [PREPARATION](PREPARATION.md)     | antoine.maille.etu@univ-lille.fr<br/>remy.pierrez.etu@univ-lille.fr<br/>adrian.rosin.etu@univ-lille.fr<br/>nestor.skoczylas.etu@univ-lille.fr                                               |
| J (E-SERVICES) | [RETRAIT](RETRAIT.md)             | harald.frebourg.etu@univ-lille.fr<br/>benjamin.niddam.etu@univ-lille.fr<br/>justin.sillou.etu@univ-lille.fr                                                                                 |
