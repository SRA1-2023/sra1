# Gestions des clients

## En tant que client je peux créer un compte client afin de commander

Le client doit saisir son nom, prénom, adresse complète, email, mot de passe.

Lors de la création de compte, l'adresse e-mail doit être valide.

Pour contrôler cette adresse, un mail sera envoyé au client avec un lien de confirmation valable 24h.

Lorsque le client a cliqué sur ce lien son compte est "activé", si cet action a lieu moins de 24h après l'envoi.

Il pourra demander un nouveau lien d'activation.

*Modification.*

Un client a la possibilité de modifier ses coordonnées (nom, prénom, adresse, email).

Si il change son adresse e-mail, il devra confirmer par un lien d'activation ce nouvel email. L'ancien email indiqué restera actif tant que la nouvelle adresse n'a pas été confirmée.

*Mot de passe.*

Vous controlerez que le mot de passe respecte un certains nombre
de critères habituels :

* Taille supérieur ou égale à 8 caractères
* Au moins une majuscule
* Au moins un chiffre

Prenez soin de hasher et saler les mots de passe en base de
données.

## En tant que client je peux commander des articles.

Après avoir mis mes articles au paniers, je dois pouvoir valider ma commande.

Le client ne peut passer des commandes que si son compte est activé, dans le cas contraire, le client peut se logger mais un avertissement lui indique qu'il n'a pas encore activé son compte.

La commande est associée à ce client.

*Historique.*

Le client peut voir l'historique de ses commandes.

Une page de synthèse affiche le numéro de commande, la date et le montant total.

Pour chaque commande, je peux voir le détail de chaque produit avec les quantités commandées.

Evidemment, un client ne pourra voir que ses commandes, il ne doit pas être possible de voir les commandes d'un autre client.

## En tant que client je peux adresser une réclamation.

Après m'être identifié, un formulaire de contact me permet d'ouvrir une réclamation.

Je peux indiquer un numéro de commande et un texte précisant l'objet de ma réclamation.

Cette réclamation passe en status "en attente de réponse".

Je peux consulter l'historiques des messages échangés autours d'une réclamation que j'ai envoyé.

Je peux toujours ajouter du détail sur une réclamation ouverte "en attente de réponse".

Je ne peux plus ajouter de message ou de détail lorsque la réclamation passe en état "cloturée".

## En tant qu'admin je peux répondre à une réclamation et cloturer une demande.

Après m'être identifié, une liste des réclamations "en attente de réponse" me sont présentées.

Je peux consulter et répondre à une réclamation par un nouveau commentaire.

Cette réclamation passe en status "réponse apportée".

Les échanges (commentaires) sont datés et cette date sera affiché avec les commentaires apportés.

Un admin peut considérer qu'une réclamation peut être "cloturée" et mettre une réclamation dans ce status.

## En tant qu'admin je peux "bannir" un compte client. 

Un admin peut rechercher les informations d'un compte client.

Il pourra consulter les informations du compte client (nom, prénom, email).

Il peut "bannir" un compte client, ce dernier ne pourra plus s'identifier ou tenter de re-créer un compte avec cette adresse e-mail. 
