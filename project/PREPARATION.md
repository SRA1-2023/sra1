# Préparation des commandes.

## En tant qu'admin, je peux préciser où se trouvent les produits dans l'entrepôt afin de les retrouver facilement pour constituer les commandes.

Modifier le modèle pour permettre de persister les données d'allée et d'emplacement. Une allée est une lettre (A .. Z). Un emplacement est un nombre (00 .. 99).

Modifier les pages d'administration des produits pour permettre la saisie de localisation des produits.

Afin de faciliter la saisie, faîtes une liste des produits dont l'emplacement n'est pas encore défini.

Réfléchissez à une interface qui permet la saisie rapide des emplacements pour les différents produits.

Une localisation (allée + emplacement) ne peut être affectée qu'à un seul produit.

Ajoutez aussi cette contrainte et affichez un message d'erreur à l'utilisateur si il essaie d'utiliser deux fois le même emplacement.

Votre message d'erreur indiquera quel produit se trouve déjà à cet endroit.

## En tant qu'admin, je peux préciser le niveau de température pour la conservation des produits et gérer des zones de stockage liées à un niveau pour la préparation des commandes.

Il existe trois niveaux de conservation : "courant", "frais", "surgelés".

Modifiez le modèle et permettez la saisie et la modification du niveau de conservation température des produits.

Ajoutez également des zones de préparations liées à un niveau de température.

Une zone de préparations possèdera un nom, un numéro et une capacité (en nombre de casiers).

Il sera possible de lister, ajouter, modifier ou supprimer une zone de préparation.

## En tant qu'employé, je peux visualiser la liste des produits à prélever dans les allées afin d'optimiser la préparation des prochaines commandes.

Sur la base de la liste des produit à livrer dans les 4 prochaines heures (qui n'auraient pas déjà été prélevés), effectuez une liste de tous les produits à retirer des allées en indiquant : la référence produit, le libellé, l'allée, l'emplacement, la quantité totale à retirer, la zone de conservation.

Effectuez une interface qui permette de valider le picking d'une allée complète.

Il devra être facile d'identifier les allées pour lesquelles la préparation n'est pas terminée.

Une fois le picking effectué, on considère que les produits se trouvent dans la zone d'attente de préparation, dans un compartiment adapté à leur conservation selon le niveau de température.

Ces zones d'attente ne sont pas à gérer par le système, il n'y en a qu'une par niveau de température et les employés retrouveront facilement les produits concernés pour la préparation des commandes. 

## En tant qu'employé, je peux préparer les commandes à livrer dans les prochaines 60 minutes.

Si ce n'est pas déjà fais, l'employé pourra sélectionner son nom parmi une liste déroulante (issue d'une table des employés).

Cette information pourra être conservée en session et l'employé pourra se "déconnecter" quand il le souhaite.

Afficher la liste des commandes à préparer, permettez d'en sélectionner une.

1. Un second écran permettra de commencer la préparation de la commande.
2. Saisir un numéro de casier, puis présenter une liste des produits à préparer pour une commande et un niveau de température.
Cette liste affichera :
la référence, le libellé, la quantité.
3. Je viens cocher les produits au fur et à mesure que je les range dans le casiers.

A chaque fois que je coche une case, je dois mettre à jour les informations en associant ce produit commandé avec le casier dans lequel je l'ai rangé.

L'employé coche les produits jusqu'à ce que le casier soit plein, dans ce cas il viendra cliquer sur un bouton et reprendra à l'étape 2 avec les produits suivants.

Il effectue ces actions jusqu'à ce qu'il n'y ait plus de produit à ranger dans le casier pour ce niveau de conservation.

4. Passer au niveau de température suivant pour cette commande et reprendre à l'étape 2 jusqu'à avoir traité tous les produits.

Une fois tous les produits rangés dans les casiers, la commande change de statut et devient "prête à livrer".

## En tant qu'admin du drive, je peux avoir des statistiques sur les préparations de commandes.

L'admin commencera par choisir une période pour ses statistiques (date début, date de fin).

Pour cette période, vous commencerez par afficher le nombre de commandes préparées par employé.

Vous afficherez également, pour chaque jour, le nombre de commandes et le chiffre d'affaire réalisé.
