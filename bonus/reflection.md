# La réflexivité

La réflexivité est un mécanisme qui permet de découvrir la structure interne des programmes. Les programmes sont ainsi capables de s'auto-décrire.

La vidéo de cours suivante présente le mécanisme de réflexivité du langage de programmation Java.

[![La réflexivité](http://img.youtube.com/vi/7ncN7_N5nXU/0.jpg)](https://www.youtube.com/watch?v=7ncN7_N5nXU "La réflexivité")

[Les transparents de la vidéo](slides/reflection.pdf)
