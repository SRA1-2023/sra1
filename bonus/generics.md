# Les types génériques

La vidéo de cours suivante présente la notion de type générique du langage de programmation Java.

[![L'API Collection](http://img.youtube.com/vi/93HsY_uCau0/0.jpg)](https://www.youtube.com/watch?v=93HsY_uCau0 "L'API Collection")

[Les transparents de la vidéo](slides/generics.pdf)
