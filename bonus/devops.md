# DevOps

La vidéo de cours suivante présente les principes DevOps pour le développement d'applications.

[![DevOps](http://img.youtube.com/vi/fN0dfAmTMwM/0.jpg)](https://www.youtube.com/watch?v=fN0dfAmTMwM "DevOps")

[Les transparents de la vidéo](slides/devops.pdf)
