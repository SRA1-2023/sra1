# Architecture Hexagonale / Clean Architecture / "Onion Architecture"

## Description

L'architecture hexagonale divise l'application en couches concentriques ou en oignon. Au centre se trouve le domaine ou le cœur de l'application, qui contient la logique métier de l'application. Les couches externes fournissent des services pour l'application, en agissant comme des passerelles pour les couches internes.

Les couches externes incluent généralement une couche de présentation, une couche de services d'application et une couche d'infrastructure. La couche de présentation est responsable de l'interface utilisateur de l'application, la couche de services d'application fournit des services applicatifs pour les couches supérieures, et la couche d'infrastructure fournit des services techniques pour l'application.

La caractéristique clé de l'architecture en oignon est la direction de la dépendance. Les couches externes dépendent des couches internes, mais les couches internes ne dépendent d'aucune couche externe. Cela garantit que le code de l'application est hautement modulaire et facilement testable.

En résumé, l'Onion Architecture est une architecture en couches qui met l'accent sur la séparation des préoccupations et la modularité du code. Elle permet de développer des applications évolutives, maintenables et testables, grâce à une conception modulaire et à la direction de la dépendance.

![Onion Architecture](./assets/onion-architecture.png)

## Et concrètement

Le concept de clean architecture peut être abstrait tant qu'on ne l'a pas mis en pratique. Voici un exemple concret d'implémentation sur base de Spring.

![Clean Architecture](./assets/hexagonal_architecture.png)

### 1. Réception d'un événement métier

Lorsqu'un événement métier est émis, un adapter en entrée de notre application va récupérer cet événement et le sérialiser dans un format compréhensible par notre application. Dans notre cas, nous utilisons un adapter Spring Kafka, annoté ```@KafkaListener```.

```java
@Component
public class EventListener {

    private ServiceMetier serviceMetier;
    public EventListener(ServiceMetier serviceMetier) {
        this.serviceMetier = serviceMetier;
    }

    @KafkaListener(topics = "fr-prisme-event-v1")
    public void onEvent(@Valid Event event) {
        serviceMetier.process(event);
    }
}
```

Afin de ne pas polluer le Bounded Context de son application avec des objets que l'on ne va pas manipuler, nous allons mettre en place un ACL (Adapters and Converters Layer) qui permettra de convertir l'événement en un objet métier. Pour notre exemple, nous allons utiliser une classe MapStruct annotée ```@Mapper``` qui permettra de faire le mapping entre l'événement et l'objet métier. Elle sera injectée dans notre Domain Service qui sera une interface dans un premier temps. Ce mapper est un composant de type Application Service.

```java
@Mapper(componentModel = "spring")
public interface EventMapper {
    ObjetMetier convertirMonEvenement(Event event);
}

public interface ServiceMetier {
    ActeMetier traiter(ObjetMetier objetMetier);
}

@Component
public class EventListener {

    private EventMapper eventMapper;
    private ServiceMetier serviceMetier;
    public EventListener(EventMapper eventMapper, ServiceMetier serviceMetier) {
        this.eventMapper = eventMapper;
        this.serviceMetier = serviceMetier;
    }

    @KafkaListener(topics = "fr-prisme-event-v1")
    public void onEvent(@Valid Event event) {
        ObjetMetier monObjetMetier = eventMapper.convertirMonEvenement(event);
        serviceMetier.traiter(monObjetMetier);
    }
}
```

### 2. Traiter l'événement

Le Domain est le cœur de notre application, il contient la logique métier de notre application. Le Domain ne doit pas dépendre de librairies externes ou d'autres composants de l'application, il doit être le plus pur possible. La dépendance se fait donc dans le sens inverse, c'est-à-dire que notre Domain Service sera injecté dans notre ACL. Il est composé de plusieurs couches :

- Domain Model : contient les objets métiers de notre application
- Domain Service : contient la logique métier de notre application

```java
@Service
public class ServiceMetierLogique implements ServiceMetier {

    public ActeMetier traiter(ObjetMetier objetMetier) {
        // ...
    }
}
```

### 3. Persister notre sortie

Prenons pour postulat que l'acte métier en sortie de notre application doit être persisté dans un premier temps. Pour cela, nous allons définir un Domain Service qui sera une interface. L'implémentation sera réalisée avec JPA et Spring Data et injectée dans notre Domain Service.

```java
public interface ReferentielActesMetier {
    ActeMetier sauvegarde(ActeMetier acteMetier);
}

@Service
public class ServiceMetierLogique implements ServiceMetier {

    private ReferentielActesMetier referentielActesMetier;
    public ServiceMetierLogique(ReferentielActesMetier referentielActesMetier) {
        this.referentielActesMetier = referentielActesMetier;
    }

    public ActeMetier traiter(ObjetMetier objetMetier) {
        // ...
        referentielActesMetier.sauvegarde(acteMetier);
    }
}

@Service
public class ReferentielActesMetierPersistence implements ReferentielActesMetier {

    private ActesMetierRepository actesMetierRepository;
    private ActeMetierMapper acteMetierMapper;
    public ReferentielActesMetierImpl(ActesMetierRepository actesMetierRepository, ActeMetierMapper acteMetierMapper) {
        this.actesMetierRepository = actesMetierRepository;
        this.acteMetierMapper = acteMetierMapper;
    }

    public ActeMetierEntity sauvegarde(ActeMetier acteMetier) {
        ActeMetierEntity acteMetierEntity = acteMetierMapper.convertirEnEntity(acteMetier);
        return actesMetierRepository.save(acteMetier);
    }
}

@Repository
public interface ActesMetierRepository extends CrudRepository<ActeMetier, String> {
}
```

L'utilisation d'une interface permet de découpler notre Domain Service de l'implémentation. Cela permet de changer d'implémentation sans toucher au Domain Service. On peut tout à fait envisager d'utiliser une implémentation InMemory pour les tests par exemple.

```java
@Profile("test")
public class ReferentielActesMetierInMemory implements ReferentielActesMetier {

    private Map<String, ActeMetier> actesMetier = new HashMap<>();

    public ActeMetier sauvegarde(ActeMetier acteMetier) {
        actesMetier.put(UUID.randomUUID().toString(), acteMetier);
        return acteMetier;
    }
}
```

### 4. Ajout d'une nouvelle fonctionnalité

Imaginons que nous devons ajouter une nouvelle fonctionnalité à notre application, nous devons donner la possibilité de réceptionner un évènement métier directement depuis une API REST. Pour cela, nous allons créer un nouveau adapter en entrée de notre application qui sera un controller Spring annoté ```@RestController```.

```java
@Mapper(componentModel = "spring")
public interface BusinessMapper {
    ObjetMetier convertirMaCommande(BusinessCommand businessCommand);
}

@RestController
public class BusinessController {

    private BusinessMapper businessMapper;
    private ServiceMetier serviceMetier;
    public EventListener(BusinessMapper businessMapper, ServiceMetier serviceMetier) {
        this.eventMapper = eventMapper;
        this.serviceMetier = serviceMetier;
    }

    @PostMapping("/events-business")
    public void postCommand(@Valid @RequestBody BusinessCommand businessCommand) {
        ObjetMetier monObjetMetier = businessMapper.convertirMaCommande(businessCommand);
        serviceMetier.traiter(monObjetMetier);
    }
}
```

Le cœur logique de notre application ne change pas, il suffit de créer un nouveau Mapper qui permettra de convertir notre commande en objet métier.

### 5. Ajout d'une nouvelle sortie

De la même manière, le besoin peut évoluer pour ajouter une nouvelle sortie à notre application, un fichier par exemple. Il faudra toujours définir un Domain Service correspondant à notre besoin.

```java
public interface ExportActesMetier {
    void exporte(ActeMetier acteMetier);
}

@Service
public class ExportActesMetierFichier implements ExportActesMetier {

    public void exporte(ActeMetier acteMetier) {
        // ...
    }
}
```

# Structure de l'application

## Description

L'application est découpée en plusieurs modules :

```
metier-domain
    |_ src/main/java
        |_ fr.axa.metier
            |_ domain
                |_ model
                |   |_ ObjetMetier.java
                |   |_ ActeMetier.java
                |_ service
                    |_ ServiceMetier.java
                    |_ ReferentielActesMetier.java
                    |_ ExportActesMetier.java

metier-application
    |_ src/main/java
        |_ fr.axa.metier
            |_ adapters
            |   |_ controller
            |   |   |_ BusinessController.java
            |   |_ listener
            |       |_ EventListener.java
            |_ application
                |_ model
                |   |_ BusinessCommand.java
                |_ mapper
                    |_ BusinessMapper.java
                    |_ EventMapper.java
                    |_ ActeMetierMapper.java

metier-adapters
    |_ metier-persistence
    |   |_ src/main/java
    |       |_ fr.axa.metier
    |           |_ adapters
    |               |_ persistence
    |                   |_ repository
    |                   |   |_ ActesMetierRepository.java
    |                   |_ ReferentielActesMetierPersistence.java
    |                   |_ model
    |                   |   |_ ActeMetierEntity.java
    |                   |_ config
    |                       |_ PersistenceConfig.java
    |_ metier-inmemory
    |   |_ src/main/java
    |       |_ fr.axa.metier
    |           |_ adapters
    |               |_ persistence
    |                   |_ ReferentielActesMetierInMemory.java
    |_ metier-export
        |_ src/main/java
            |_ fr.axa.metier
                |_ adapters
                    |_ export
                        |_ ExportActesMetierFichier.java
```

