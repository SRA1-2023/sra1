# DDD

# Introduction

Le Domain-Driven Design (DDD) est une approche de conception logicielle qui vise à modéliser le domaine métier au cœur de l'application.
Cela implique de comprendre en profondeur les concepts et les règles du domaine pour créer un modèle logiciel qui reflète fidèlement la réalité métier.
L'objectif principal de DDD est de créer des logiciels complexes, mais compréhensibles, maintenables et évolutifs en alignant le code sur le langage du métier.
En utilisant un langage commun entre les développeurs et les experts du domaine, DDD facilite la collaboration et améliore la qualité des logiciels en résolvant les problèmes métier de manière élégante et claire.

# Quand l'utiliser ?

DDD est une approche de conception logicielle qui peut être utilisée dans n'importe quel projet, mais elle est particulièrement utile pour les projets complexes où la logique métier est complexe et difficile à comprendre.

Parfois en tant que développeur, nous avons tendance à nous concentrer sur la technologie et à oublier le métier. Cela peut entraîner des problèmes de communication avec les experts du domaine et des problèmes de qualité du code.

On peut faire le penchant avec DevOps qui permet de faire une passerelle entre les développeurs et les opérationnels. DDD permet de faire la même chose entre les développeurs et les experts du domaine.

On se recentre sur ce dont a vraiment besoin le métier.

Par contre, il ne faut pas considérer DDD comme une pratique à appliquer à la lettre. Il faut l'adapter à chaque projet et à chaque équipe. D'ailleurs certains concepts sont déjà largement utilisés au sein d'Axa comme le BDD (Behavior Driven Development) et les Events Storming.

L'utilisation de la langue maternelle du métier, dans notre cas le Français, est un élément clé de DDD. Cela permet de créer un langage commun entre les développeurs et les experts du domaine, un modèle logiciel qui reflète fidèlement la réalité métier, un code affordant. Encore une fois ce n'est pas applicable à tous les domaines mais dans le monde de l'assurance, c'est un vrai plus puisque l'on utilise des termes endémiques à la France et difficilement traduisibles.

# La Théorie

## Patterns stratégiques de DDD

Ce sont les patterns les plus importants. Ils permettent de définir les limites du domaine et de définir les interactions entre les différents domaines.

![DDD strategic pattern](./assets/ddd_strategic_pattern.png)

### Ubiquitous Language

C'est le langage commun entre les développeurs et les experts du domaine. Il est utilisé dans toutes les couches de l'application. Il permet de créer un modèle logiciel qui reflète fidèlement la réalité métier.

![https://jpattonassociates.com/glad-we-all-agree-2/](./assets/glad-we-all-agree-1.png)

### Bounded Context

C'est une limite logique qui délimite un domaine. Il permet de définir un contexte métier et un langage commun. Il peut être défini par une équipe, un service ou une application. Il peut être décomposé en plusieurs sous-domaines.

#### 1 base de code = 1 Bounded Context

Cela permet de délimiter les responsabilités de chaque application et de définir les interactions entre les applications.

#### 1 Bounded Context = 1 Ubiquitous Language

C’est une notion très importante car le même nom ne se modélise pas forcément de la même façon dans chaque contexte borné. Par exemple, le terme « client » n’a pas la même signification dans le contexte de la vente et dans le contexte de la comptabilité. Dans le premier cas, il s’agit d’une personne qui achète un produit ou un service, dans le second cas, il s’agit d’une personne qui a un compte ouvert dans l’entreprise. Il est donc important de bien définir le contexte dans lequel on se trouve pour éviter les confusions.

![Bounded Context](./assets/bounded_context.png)

#### 1 Bounded Context = 1 équipe

Cela permet de définir les responsabilités de chaque équipe et de définir les interactions entre les équipes. Cela permet également de définir les limites de chaque équipe.

### Context Map

C'est une représentation graphique des différents Bounded Contexts et de leurs interactions. Il permet de définir les relations entre les différents Bounded Contexts.

### Shared Kernel

C'est un ensemble de classes partagées entre plusieurs Bounded Contexts. Il permet de définir les interactions entre les différents Bounded Contexts. Par exemple un Value Object ```Email``` peut être utilisable par plusieurs contextes bornés dans l’application.

## Patterns tactiques de DDD

![DDD tactical pattern](./assets/ddd_tactical_pattern.png)

### Value Object

C'est la donnée la plus petite possible comportant une valeur pour le métier. Ce dernier doit être obligatoirement immutable.

Par exemple : un genre, une devise, une adresse ; tout ceci peut être partageable, ça n’a pas d’existence propre.

#### Les avantages d’un Value Object

- Découper les entités plus proprement
- Dégager de la complexité métier dans une classe dédiée
- Exprimer le domaine métier plus facilement
- Garder la complexité d’un objet à l’intérieur de ce dernier

#### Comment créer un objet-valeur ? 

- Immutable (pas de setter – tout se fait dans le constructeur)
- Riche en logique métier : on crée les fonctions du VO en accord notre comportement métier
- Complètement défini par l’ensemble de ses attributs (si on concatène l’ensemble des champs on doit pouvoir la comparer avec une autre instance) – égalité forte, unicité forte

### Entity

Une entité est un objet composé de plusieurs value object. Il est composé de plusieurs données qui lorsqu’elles sont réunies permettent de faire un process donné.

### Repository

Avec ce Design Pattern Repository, on se sert d'une interface pour accéder aux données, peu importe où elles se trouvent.

C’est un gros avantage, car le métier se fiche complètement d’où se trouve la donnée à l’instant T (API, Redis, Base de données, fichiers sur le disque…).

Grâce au Repository, on est capable de ne pas tenir compte de l’accès à la donnée depuis notre domaine. Aussi, si jamais il y a besoin de changer de système de données, on a plus qu’à plugger l’interface repository et ça y est, on est passé de MySQL à PostgreSQL.

### Aggregates

C'est une collection de d'entité qui ont un sens métier lorsqu'ils sont réunis.

<strong>Exemple:</strong> Une commande dans le e-commerce à besoin de la liste des produits commandés pour faire n'importe qu'elle opération (calcul du prix, calcul du poids, etc). Donc l'objet commande et ces différents ligne de commandes seront un agrégat.

### Services

Lorsqu'un processus ou une transformation importante dans le domaine n’est pas une responsabilité naturelle d’un objet entité ou valeur. Alors on utilise la notion de service pour englober ces processus. Leur seul but est de fournir une fonctionnalité (souvent au domaine).

### Domain Events

Un événement de domaine est une partie à part entière du modèle de domaine, une représentation de quelque chose qui s’est passé dans le domaine. 
Ignorer l’activité non pertinente du domaine tout en rendant explicite les événements que les experts du domaine souhaitent écouter ou être informés, ou qui sont associés
avec changement d’état dans les autres objets du modèle

### Factory

Transférer la responsabilité de la création d’instances d’objets et d’agrégats complexes à un
objet séparé, qui n’a aucune responsabilité dans le modèle de domaine mais qui fait partie de la conception du domaine. Parfois certains objets sont plutôt complexes à créer et nécessitent un peu plus de traitement qu’un simple ```new``` devant la classe à instancier.

Un builder ou constructeur est un service qui permet d'instancier des objets qui peuvent nécessiter d'être construits avec certaines dépendances ou certains paramètres qui peuvent sortir de notre Bounded Context, c'est une usine de création d'objets.

# La Pratique

## Annotation

Pour structurer son code et donner un visuel assez claire sur les types d'objects.
On utilise une annotation pour chaque type d'object, toute les annotations seront bien document pour délimité l'utilisation et la responsabilité de chaque type énoncé (les définitions peuvent changer ou être adapté à chaque domaine).

## Taille d'un domaine

Dès lors qu'un domaine dépasse une certaine taille (à définir lorsque l'on aura mis en place les 1ère briques), il faut penser à le scinder en plusieurs domaines ayant du sens. 

# Références

Si vous voulez aller plus loin, voici quelques références :

[Domain Driven Design Part I: Strategic Design and Integration Patterns | by Can Güt | Medium](https://medium.com/@lifelongstudent/domain-driven-design-part-i-strategic-design-and-integration-patterns-1ed169543c8a)

[DDD : Domain-Driven Design (Théorie & Pratique) – Alex so yes](https://alexsoyes.com/ddd-domain-driven-design/)

[[SC] Démystifions le Domain Driven Design - Maxime Sanglan-Charlier - YouTube](https://www.youtube.com/watch?v=O2X7fH043gk)

[DDD: et si on reprenait l'histoire par le bon bout ? (Thomas Pierrain - Jérémie Grodziski) - YouTube](https://www.youtube.com/watch?v=o3thkx3EuiA)

[Microservices, DDD et bootstrapping pour faire un départ lancé (Laurent Guérin et Aurélien Brisard) - YouTube](https://www.youtube.com/watch?v=CXaLEyckqu8)

[DDD, en vrai pour le développeur (Cyrille Martraire) - YouTube](https://www.youtube.com/watch?v=h3DLKrvp5V8)

[DDD Vite Fait - pdf](http://seedstack.org/pdf/DDDViteFait.pdf)

[Martin Fowler - blog](https://martinfowler.com/tags/domain%20driven%20design.html)

[High-speed DDD (Thomas PIERRAIN) - YouTube](https://www.youtube.com/watch?v=tAGG4-UtfGw)