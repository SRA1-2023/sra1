# Docker

Docker permet de créer des environnements isolés, appelés containers, pour y exécuter des processus ou applications, de manière plus légère que la virtualisation. La vidéo suivante présente les grandes notions de docker.

[![Docker](http://img.youtube.com/vi/nEcjF0MAPtw/0.jpg)](https://www.youtube.com/watch?v=nEcjF0MAPtw "Docker")

[Les transparents de la vidéo](slides/devops.pdf)


La vidéo de démonstration suivante présente l'utilisation basique de Docker.

Nous y voyons les éléments suivants :

- la recherche d'une image sur le "Docker Hub" : https://hub.docker.com,
- le téléchargement d'une image de base de données,
- le démarrage d'un container pour la base de données PostgreSQL,
- la connexion d'une application à cette base de données.

[![Screencast Docker](http://img.youtube.com/vi/hATHIb-p2hw/0.jpg)](https://www.youtube.com/watch?v=hATHIb-p2hw "Screencast Docker")

Pour pratiquer un peu l'outil Docker, nous vous proposons deux exercices.


## Exercice Docker #1

Docker et les bases de données.

- Récupérer l'image Docker de la base de données PostgreSQL.
- Démarrer une base de données PostgreSQL avec Docker.
- Connecter votre application Servlet/JSP/JDBC à la base de données démarrée avec Docker.

Le screencast peut vous aider dans la réalisation de cet exercice.


## Exercice Docker #2

Démarrer votre application servlet avec Docker.

- Construire un fichier .war contenant votre application.
- Créer un container en y montant votre fichier .war dans le répertoire /usr/local/tomcat/webapps/ du container:
    - `docker container create --name monappli -v 'monfichier.war:/usr/local/tomcat/webapps/monfichier.war' -p 8080:8080 tomcat:9`
    - `docker container start monappli`

ou

- Créer un Dockerfile qui crée une nouvelle image Docker contenant votre fichier .war

```
FROM tomcat:9
COPY monfichier.war /usr/local/tomcat/webapps/
```


- et construire puis exécuter votre image Docker :
    - `docker build -t monimage .`
    - `docker container create --name monappli -p 8080:8080 monimage`
    - `docker container start monappli`
