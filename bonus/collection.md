# L'API Collection

L'API Collection est une librairie de classes et d'interfaces qui permettent de manipuler des structures de données. De nombreuses structures sont disponibles. La vidéo de cours se concentre sur trois qui sont parmi les plus utilisées : les listes (List), les ensembles (Set), les tables de hachage (Map).

[![L'API Collection](http://img.youtube.com/vi/cg0r-u8fYsQ/0.jpg)](https://www.youtube.com/watch?v=cg0r-u8fYsQ "L'API Collection")

[Les transparents de la vidéo](slides/collection.pdf)
