# Les annotations

Il s'agit d'un mécanisme qui permet de qualifier les instructions d'un programme en ajoutant des informations (on parle souvent de métadonnées) qui enrichissent la description de ce programme. Les annotations sont apparues avec la version 5 du langage Java. Les annotations s'utilisent avec en utilisant le symbole @ que l'on place devant certaines ligne de code d'un programme.

La vidéo de cours suivante présente le mécanisme des annotations du langage de programmation Java.

[![Les annotations Java](http://img.youtube.com/vi/EkFF8f376gQ/0.jpg)](https://www.youtube.com/watch?v=EkFF8f376gQ "Les annotations Java")

[Les transparents de la vidéo](slides/annotations.pdf)
