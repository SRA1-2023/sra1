# Intégration et déploiement continus

La vidéo de cours suivante présente les mécanismes de déploiement et d'intégration continue du langage Java.

Deux screencasts présentent également les produits d'intégration continue Gitlab-CI et Github Actions, en prenant pour exemple l'intégration continue d'une application Java EE avec Maven.

[![CI-CD](http://img.youtube.com/vi/tGqXTaoWufs/0.jpg)](https://www.youtube.com/watch?v=tGqXTaoWufs "CI-CD")

[Les transparents de la vidéo](slides/ci-cd.pdf)

La vidéo suivante présente l'implémentation d'un pipeline d'intégration continue simple avec Gitlab-CI pour un projet Maven :

- création d'un projet sur Gitlab, et envoi du code,
- écriture d'un fichier yaml pour définir le pipeline d'intégration continue,
- production d'un fichier war en sortie du pipeline.

[![GitLab-CI](http://img.youtube.com/vi/koCxvagKgDU/0.jpg)](https://www.youtube.com/watch?v=koCxvagKgDU "GitLab-CI")

La vidéo suivante présente l'implémentation d'un pipeline d'intégration continue simple avec Github Actions pour un projet Maven :

- création d'un projet sur Github et envoi du code,
- écriture d'un fichier yaml pour définir le workflow d'intégration continue,
- production d'un fichier war en sortie du pipeline.

[![GitHub Actions](http://img.youtube.com/vi/-esekAPyWTY/0.jpg)](https://www.youtube.com/watch?v=-esekAPyWTY "GitHub Actions")

En guise d'exercice, nous vous proposons de :

- créer un repository sur github.com ou gitlab.com et y publier votre code source,
- développer un pipeline d'intégration continue en utilisant GitLab-CI ou GitHub Actions pour construire votre fichier war.
